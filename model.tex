\documentclass{lmcs}

\usepackage{enumerate}
\usepackage{hyperref}


\usepackage[all]{xy}
\usepackage{mathpartir}
\usepackage{amsmath}
\usepackage{ulem}
\usepackage{graphics}
\usepackage[usenames]{color}
\usepackage{amssymb}
%\usepackage{theorem}
\usepackage{prooftree}
\usepackage{float}

\newtheorem{theorem}[thm]{Theorem}

\newtheorem{lemma}[thm]{Lemma}
%\newtheorem{fact}[thm]{Fact}
\newtheorem{definition}[thm]{Definition}
\newtheorem{proposition}[thm]{Proposition}
\newtheorem{corollary}[thm]{Corollary}
\newtheorem{remark}[thm]{Remark}
\newtheorem{property}[thm]{Property}
\newtheorem{example}[thm]{Example}
\newtheorem{notation}[thm]{Notation}


\newcommand{\cal}[1]{\mathcal #1}

\input{macrosModel}


\begin{document}

\title{A lambda model}


\author{Antonio Bucciarelli}
\address{IRIF, CNRS and Univ Paris-Diderot, France  }	
\email{buccia@irif.fr} 
%\thanks{thanks 1, optional.}	%optional

\author{ Delia Kesner }	%optional
\address{IRIF, CNRS and Univ Paris-Diderot, France  }
\email{kesner@irif.fr}  %optional
%\thanks{thanks 2, optional.}	%optional

\author{Simona Ronchi Della Rocca}	%optional
\address{Dipartimento di Informatica, Universit\`a di Torino, Italy}	%optional
\email{ronchi@di.unito.it}  %optional
%\thanks{thanks 3, optional.}	%optional

%% mandatory lists of keywords and classifications:
\keywords{Lambda-calculus, Type-Assignement Systems, Non-idempotent Intersection Types, Inhabitation Problem}
\subjclass{F.4.1 Mathematical Logic: Lambda calculus and related systems, Proof theory. F.3.1 Specifying and Verifying and Reasoning about Programs: Logics of programs}
%\titlecomment{Revised and extended version of \cite{DBLP:conf/ifipTCS/BucciarelliKR14}.}
%OPTIONAL comment concerning the title, \eg, if a variant
%or an extended abstract of the paper has appeared elsewehere}



\section{Type system}\label{sec:hn}

% In order to make decidable the inhabitation
% problem we use non-idempotent intersection types (so enjoying
% associativity and commutativity, but not idempotency). 
The typing rules are relevant (\ie\ the weakening is not allowed), 
and we represent
non-idempotent intersections as multisets of types. Moreover,
in order to work with a syntax-directed system, we 
restrict the set of types to the {\it strict ones}~\cite{Bakel92}. 

\begin{definition}
\label{def::lambdasys} \mbox{}
\begin{enumerate}
\item The set of {\it types}  is defined by the following grammar:
\[ \begin{array}{llllll}
\sigma, \tau, \rho & ::= & \alpha \mid \A \arrow \tau & (\mbox{types})  \\  
\A, \D  & ::= & \multiset{\sigma_{i}}_{\iI}  &  (\mbox{multiset types}) 
\end{array} \] 
where $\alpha$ ranges over a countable set of base types.
Multiset types are associated to a finite, 
possibly empty, set $I$ of indices;  the {\it empty multiset} corresponds to the case $I=\emptyset$
and  is simply denoted by $\emm$. To avoid an excessive number of parentheses,
we  use for example $\A \arrow \D \arrow \sigma$ instead of $\A \arrow (\D \arrow \sigma)$.
\ignore{ \item The  non-deterministic
{\it choice} operator on multiset types is defined as follows:
\[\A^*   := \left \{ \begin{array}{lllll}
                      \A & \mbox{ if } \A \neq \emul \\
                      \multiset{\tau}  &  \mbox{ if } \A = \emul,  \mbox{ where } \tau \mbox{ is an arbitrary type} \\
\end{array}  \right . \]
This operator will play a special role in Section~\ref{section:sn}. }


\item {\it Typing environments}, which are also simply called environments, written
  $\Gamma, \Delta$, are functions from variables to multiset types,
  assigning the empty multiset  to almost all the
  variables. We use the symbol $\es$ to denote the empty typing environment. The domain of $\Gamma$, written $\dom{\Gam}$, is the set
  of variables whose image is different from $\emul$. Given
  environments $\Gamma$ and $\Delta$, $\Gamma + \Delta$ is the
  environment mapping $\x$ to $\Gam (\x)\uplus\Delta(\x)$, where
  $\uplus$ denotes multiset union; $+_{\iI} \Delta_i$ denotes  its obvious
  extension to a non-binary case, where the resulting environment 
  has empty domain in the case  $I=\es$.
We write $\Gamma\sm \x$ for 
the environment assigning $\emul$ to $\x$, and acting as $\Gamma$
otherwise; ${\x}_1:{\A}_1,\ldots,{\x}_n:{\A}_n$ is the environment
assigning ${\A}_i$ to ${\x}_i$, for $1\leq i\leq n$, and $\emul$ to
any other variable. 

\item A {\it typing judgement} is a triple either of the form $\Gam \vdash \s:\sig$ or $\Gam \vdash \s:\A$.
The type system $\Mu$ is given in Figure~\ref{fig:system-lambda}. 
\end{enumerate}
\end{definition}


\begin{figure}[!ht]
\begin{center}
$\begin{array}{c}
\infer{\mbox{}}{\x:\multiset{\rho} \der \x:\rho}\ (\axvar)
\quad \quad 
\infer
{\Gam\der \s:\tau }
{\Gam \sm  \x \der \l \x. \s: \Gamma(\x)\arrow \tau}\ (\arri)\\ \\
\infer{(\Delta_{i} \der \s:\sig_i)_{\iI}} {+_{\iI} \Delta_{i} \der \s:\multiset{\sig_{i}}_{\iI}}(\many)   \quad \quad 
\infer
{\Gam\der \s:	\A\arrow\tau \sep 
\Delta\der \uu:\A }
{\Gam +\Delta\der \s\uu: \tau}\ (\arre)
\end{array}$
\caption{The type assignment system $\Mu$ for the $\l$-calculus}
\end{center}
\label{fig:system-lambda}
\end{figure}

Rules $(\axvar)$ and $(\arri)$ are self explanatory.  Rule $(\many)$
can be considered as an auxiliary rule, \ie\ it has no logical meaning
but just collects together different typings for the same term; remark
that it cannot be iterated.  Rule $(\arre)$ has two premises, the one
for $\s$ (resp. $\uu$) is called the major (resp. minor) premise. In
the case $\A = \emul$, this rule allows to type a term without giving
types to all its subterms, and in particular it allows to type
an application whose argument is unsolvable.  
For example, the judgement $\x:\multiset{\emul\arrow\alpha}\der\x(\dup
\dup):\alpha$ turns to be derivable by taking $I =\emptyset$ in  rule $(\many)$ :

\[ \begin{prooftree}
\begin{prooftree}
\justifies{\x:\multiset{\emul\arrow\alpha}\der\x: \emul\arrow\alpha}
\using{(\axvar)}
\end{prooftree} \sep
\begin{prooftree}
\justifies{\emptyset \der \dup\dup :\emm }
\using{(\many)}
\end{prooftree}
\justifies{\x:\multiset{\emul\arrow\alpha}\der\x(\dup\dup):\alpha}
\using{(\arre)}
\end{prooftree} \] 
This
feature is shared by all the intersection type systems characterizing
solvability.

\begin{notation}
We write $\Pi \dem \Gam
\der_{\Mu} \s:\sigma$, or simply $\Pi \dem \Gam \der \s:\sigma$ when
$\Mu$ is clear from the context, to denote a type
  derivation $\Pi$ in system $\Mu$ with
    conclusion $\Gam \der \s:\sigma$.
We call $\s$ the {\it subject} of $\Pi$.  
A term $\s$ is said to be $\Mu$-typable if there exists a derivation $\Pi \dem \Gam
\der_{\Mu} \s:\sigma$. By abuse of notation, we omit  sometimes
the name of the derivation by writing simply  $\Gam \der
\s:\sigma$ to denote the existence of some derivation with
conclusion $\Gam \der \s:\sigma$.  We  extend these
notations to judgements of the shape $\Gam \der \s: \A$, when we want
to reason by induction on the structure of a proof.  
\end{notation}

One important tool is  going to be used in the proofs: 
\begin{defi} \label{meas-sub} \mbox{} 
%\cdelia{\begin{enumerate}
%\item \label{meas} The {\it measure} of a derivation $\Pi$, written $\meas(\Pi)$, 
%is the  number of rule applications  in $\Pi$, except  rule $(\many)$.
%\item \label{left} A derivation $\Pi$ 
%with subject $\s$ is a {\it left subderivation} of $\Sigma$  with subject $\uu$ if either
%\delia{\begin{itemize}
%\item $\Pi$ and $\Sigma$ are the same, or
%  \item the last rule of
%  $\Sig$  with subderivation $\Sig'$ is $(\arri)$,   and $\Pi$ is a left
%    subderivation of $\Sigma'$, or
%  \item the last rule of $\Sig$ with major
%    subderivation $\Sigma'$ is $(\arre)$
%      and $\Pi$ is a left subderivation of $\Sigma'$.
%\end{itemize}
%Accordingly, $\s$ is called a {\it left prefix} of $\uu$.}
%\end{enumerate}
The {\it measure} of a derivation $\Pi$, written $\meas(\Pi)$, 
is the  number of rule applications  in $\Pi$, except  rule $(\many)$.
\end{defi}


Note that the definition of the measure of a type derivation reflects
the fact that $(\many)$ is an auxiliary rule.  The notion of measure of a derivation provides an
original, combinatorial proof of the fact that typed terms do have
{\hnf}.  In fact the fundamental {\it subject reduction} property
holds as follows: if $\Pi\dem\Gamma \der\s:\sigma$ and
$\s\arrow_\beta\uu$, then $\Pi'\dem\Gamma \der\uu:\sigma$, with the
peculiarity that the measure of $\Pi'$ is strictly smaller than that
of $\Pi$ whenever the reduction $\s\arrow_\beta\uu$ takes place in a
subterm  of $\s$ which is {\it typed in $\Pi$}. A formal definition
of typed positions  follows.

\begin{definition}
  \label{def:occ}\mbox{}
\begin{itemize}

\item The set $\occ t$ of positions of  $\s$ is  the set of contexts
$\ccontext$ such that there exists a term $\uu$ verifying 
 $\ccontext[\uu]=\s$, $\uu$ being the {\it  subterm of $\s$  at position $\ccontext$}.

\item Given $\Pi\dem\Gamma\der \s:\sigma$, the {\it set $\tocc\Pi\subseteq\occ \s$ of
  typed positions of  $\s$  in  $\Pi$} is defined by induction on 
 %$\meas(\Pi)$
the structure of $\Pi$ as follows:% (referring to Figure~\ref{fig:system-lambda}):
\begin{itemize}
\item $\tocc\Pi=\set{\square}$ if $\Pi$ is an instance of the axiom.
\item $\tocc\Pi=\set{\square}\cup \set{\l\x.\ccontext\ | \ \ccontext\in \tocc{\Pi'}}$ if the 
last rule of $\Pi$ is $(\arri)$, its subject is $\l \x. \uu$  and its premise is $\Pi'$.
\item $\tocc\Pi=\set{\square}\cup \set{\ccontext\vv\ | \ \ccontext\in \tocc{\Pi'}}
\cup \set{\uu\ccontext\ |\ \ccontext\in\tocc{\Delta}}$  if the 
last rule of $\Pi$ is $(\arre)$, its subject is $\uu \vv$, $\Pi'$ and $\Delta$ are  the major and minor premises of $\Pi$ respectively.
\item $\tocc\Pi= \bigcup_{\iI}\set{\ccontext\ |\ \ccontext\in\tocc{\Pi'_i}}$ if the 
  last rule of $\Pi$ is $(\many)$, and  $(\Pi'_i)_{\iI} $ are its  premises.
A particular case is when $I = \es$, in which case
    $\tocc \Pi = \es$. This coincides with the fact that terms typed by an empty
    $(\many)$ rule are semantically untyped.
\end{itemize}

We say that {\it the subterm of $\s$ at position $\ccontext$
  is typed in $\Pi$} if $\ccontext$ is a typed 
position of $\s$ in $\Pi$.
\item  Given $\Pi\dem\Gamma\der \s:\sigma$ (resp. $\Pi\dem\Gamma\der \s:\A$), we say that $\s$ is {\it in $\Pi$-normal form},
written $\Pi\demnf\Gamma\der \s:\sigma$ (resp. $\Pi\demnf\Gamma\der \s:\A$),
if for all $\ccontext\in\tocc\Pi$, $\s= \ccontext[\uu]$ implies
$\uu$ is not a redex.
\end{itemize}
\end{definition}
 
\begin{example}\label{ex:meas}
Consider the following derivation $\Pi$, 
where $\sig:= \multiset{\alpha_0, \alpha_1} \arrow \emul \arrow \tau$:

\[ \begin{prooftree}
   \begin{prooftree}
   \begin{prooftree}
   \justifies{ \x: \multiset{\sig} \der \x: \sig}
   \using{(\axvar)}
   \end{prooftree} 
   \begin{prooftree}
   \begin{prooftree} \justifies{\y: \multiset{\alpha_0} \der \y:\alpha_0 }\using{(\axvar)}\end{prooftree} 
   \begin{prooftree} \justifies{\y: \multiset{\alpha_1} \der \y:\alpha_1 }\using{(\axvar)}\end{prooftree} 
   \justifies{\y: \multiset{\alpha_0, \alpha_1}  \der \y: \multiset{\alpha_0, \alpha_1}  }
   \using{(\many)}
   \end{prooftree} 
   \justifies{\x: \multiset{\sig}, \y: \multiset{\alpha_0, \alpha_1} \der \x \y: \emul \arrow \tau} 
   \using{(\arre)}
   \end{prooftree} 
     \begin{prooftree}
      \justifies{\es \der \iid\dup: \emul}\using{(\many)}
      \end{prooftree}
   \justifies{\x: \multiset{\sig}, \y: \multiset{\alpha_0, \alpha_1} \der \x \y (\iid\dup) : \tau}
   \using{(\arre)}
   \end{prooftree} \] 

Then, 
\begin{itemize}
\item$\meas(\Pi) = 5$.
%\csimona{\item $\Pi' \dem   \x: \multiset{\sig} \der \x: \sig $ is a left-subderivation of $\Pi$.}
\item $\tocc{\Pi} = \set{\Box, \Box\y (\iid\dup), \x \Box (\iid\dup), \Box (\iid\dup)} $.
\item $\x \y (\iid \dup)$ is in $\Pi$-normal form (since its only redex $\iid \dup$ is untyped). 
\end{itemize}
\end{example}

\begin{theorem}
\label{thr::char} \mbox{}
\begin{enumerate}
\item \label{th::red1}(Subject reduction and expansion) $\Gamma \der \s:\sigma$ and $\s =_\beta \uu$ imply $\Gamma \der \uu:\sigma$.
\item \label{th::red2} (Characterization) $\s$ is $\Mu$-typable if and only if $\s$ has  \hnf.
\end{enumerate}
\end{theorem}
\proof 
See~\cite{DeCarvalhoThesis, BKV17}. In particular, the proof of
subject reduction is based on the weighted subject reduction property that $\Pi \dem \Gamma \der
\s:\sigma$ and $\s \redb \uu$ imply $\Pi'\dem\Gamma \der \uu : \sigma$,
where $\meas(\Pi') \leq \meas(\Pi)$. Moreover, if the reduced redex 
is typed in $\Pi$, then $\meas(\Pi') <\meas(\Pi)$.
\qed


As a matter of fact, the two properties stated in the theorem above
may be proved using a semantic shortcut: in~\cite{PPR17} the class of
{\it essential} type systems is introduced, and it is shown that such
systems supply a logical description of the linear relational models
of the $\l$-calculus, in the sense of~\cite{bucciarelli07csl}. Since the
type system $\Mu$ is an instance of this class, both statements of
Theorem~\ref{thr::char} are particular cases of the results proved in~\cite{PPR17}.


\subsection{The key role of approximants}
\label{s:approximate-lambda}


System $\Mu$ assigns  types to  terms without giving types to all their
subterms (\cf\ the rule $(\arre)$ in case $\A=\emul$). So in order to
reconstruct all the possible subjects of  derivations we
need a notation for these untyped subterms, which is supplied by the
notion of approximate normal forms introduced
  in Section~\ref{sec:prelim}.
% \begin{notation}
% Let $\bigvee$ denote the least upper bound w.r.t. $\leq$.
% We use $\uparrow_{\iI}\ap_i$ to denote that $\bigvee\set{\ap_i}_{\iI}$
% does exist.  It is easy to check that, for every $\s$ and
% $\ap_1,\ldots \ap_n \in \Ap(\s)$,
% $\uparrow_{i\in\set{1,\ldots,n}}\ap_i$. An approximate normal form $\ap$
% is a {\it head subterm} of $\bp$ if either $\bp = \ap$ or $\bp=\cp
% \cp'$ and $\ap$ is a head subterm of $\cp$.
% \end{notation}
%\bdelia{ For example, $ \uparrow\set{\Omega, \l \x. \Omega, \l
%    \x\y. \Omega, \l \x\y.\y} $, and $\bigvee \set{\Omega, \l
%    \x. \Omega, \l \x\y. \Omega, \l \x\y.\y}= \l \x\y.\y$.}  It turns
%out that system $\Mu$ also gives types to approximate normal forms, by
%simply assuming that no type can be assigned to the constant $\Omega$,
%except the empty intersection type $\emul$.  
As a consequence,
  some notions previously defined for terms, naturally apply for
  approximants too. Thus for example, given $\Pi\dem\Gamma\der
  \ap:\sigma$ or $\Pi\dem\Gamma\der \ap:\A$, $\ap$
  is said to be in $\Pi$-normal form according to Definition~\ref{def:occ}.
But remark that {\it every} typing derivation of an approximant is
  necessarily in normal form.  More precisely, if $\Pi\dem\Gamma\der
  \ap:\sigma$ or $\Pi\dem\Gamma\der \ap:\A$, then $\ap$ is in $\Pi$
  normal form.  Quite surprisingly, $\Pi\dem\Gamma\der \ap:\sigma$,
  $\ap\leq \s$ and $\Pi'\dem\Gamma\der \s:\sigma$ do not imply that
  $\s$ is in $\Pi'$ normal form, as the following example shows.


\begin{example}
Let $\ap = \x \Omega (\x \y \y) \leq  \x (\iid \x) (\x \y \y) = \s$. 
Let $\Gam = \x: \multiset{\sig_1, \sig_2}, \y: \multiset{\alpha,\alpha}$, 
where $\sig_1 = \emm \arrow \multiset{\alpha} \arrow \alpha$
and $\sig_2 = \multiset{\alpha} \arrow \multiset{\alpha} \arrow \alpha$.
There are type derivations $\Pi \dem \Gam \der \ap: \alpha$ (given on the top) and
 $\Pi' \dem \Gam \der \s: \alpha$ (given in the bottom) such that
$\ap$ is in $\Pi$-normal form but $\s$ is not in
$\Pi'$-normal form. 


\[  \begin{prooftree}
    \begin{prooftree} 
   \begin{prooftree} 
   \justifies{\x: \multiset{\sig_1} \der \x: \sig_1 }
   \end{prooftree} \sep
   \begin{prooftree} 
   \justifies{\emptyset \der \Omega:\emm }
   \end{prooftree} 
   \justifies{\x: \multiset{\sig_1} \der \x \Omega: \multiset{\alpha} \arrow \alpha }
   \end{prooftree} \sep
   \begin{prooftree} 
   \begin{prooftree} 
   \begin{prooftree} 
   \begin{prooftree} 
   \justifies{\x: \multiset{\sig_2} \der \x:\sig_2}
   \end{prooftree} \sep
   \Pi_\y 
   \justifies{\x: \multiset{\sig_2}, \y: \multiset{\alpha} \der \x \y:  \multiset{\alpha} \arrow \alpha}
   \end{prooftree} \sep 
   \Pi_\y
   \justifies{\x: \multiset{\sig_2}, \y: \multiset{\alpha, \alpha} \der \x \y \y: \alpha}
   \end{prooftree}   
   \justifies{\x: \multiset{\sig_2}, \y: \multiset{\alpha, \alpha} \der \x \y \y: \multiset{ \alpha}}
   \end{prooftree}   
   \justifies{\x: \multiset{\sig_1, \sig_2}, \y: \multiset{\alpha, \alpha} \der \x \Omega (\x \y \y): \alpha}
   \end{prooftree}    \] 



\[ \begin{prooftree}
    \begin{prooftree} 
   \begin{prooftree} 
   \justifies{\x: \multiset{\sig_2} \der \x: \sig_2 }
   \end{prooftree} \sep
   \begin{prooftree} 
   \begin{prooftree}
    \begin{prooftree}
    \begin{prooftree}
   \justifies{\z: \multiset{\alpha}  \der \z:  \alpha  }
   \end{prooftree}
   \justifies{\emptyset \der \iid: \multiset{\alpha} \arrow \alpha   }
   \end{prooftree} \sep
   \Pi_\y
   \justifies{\y: \multiset{\alpha} \der \iid \y: \alpha }
   \end{prooftree} 
   \justifies{\y: \multiset{\alpha} \der \iid \y: \multiset{\alpha} }
   \end{prooftree} 
   \justifies{\x: \multiset{\sig_2},\y:\multiset{\alpha} \der \x (\iid \y): \multiset{\alpha} \arrow \alpha }
   \end{prooftree} \sep
   \begin{prooftree} 
   \begin{prooftree} 
   \begin{prooftree} 
   \begin{prooftree} 
   \justifies{\x: \multiset{\sig_1} \der \x:\sig_1}
   \end{prooftree} \sep
   \begin{prooftree} 
      \justifies{\emptyset  \der \y:  \emm  }
   \end{prooftree} 
   \justifies{\x: \multiset{\sig_1} \der \x \y:  \multiset{\alpha} \arrow \alpha}
   \end{prooftree} \sep 
   \Pi_\y 
   \justifies{\x: \multiset{\sig_1}, \y: \multiset{\alpha} \der \x \y \y: \alpha}
   \end{prooftree}   
   \justifies{\x: \multiset{\sig_1}, \y: \multiset{\alpha} \der \x \y \y: \multiset{ \alpha}}
   \end{prooftree}   
   \justifies{\x: \multiset{\sig_1, \sig_2}, \y: \multiset{\alpha, \alpha} \der \x (\iid \y) (\x \y \y): \alpha}
   \end{prooftree}    \] 

where  $\Pi_\y$ denotes the following subderivation
\[  \begin{prooftree} 
   \begin{prooftree}
   \justifies{ \y:  \multiset{\alpha}    \der \y:  \alpha   }
   \end{prooftree}
   \justifies{ \y:  \multiset{\alpha}    \der \y:  \multiset{\alpha}   }
   \end{prooftree}\] 
\end{example}



Given
$\Pi\dem \Gam \der \s:\tau$, where $\s$ is in $\Pi$-normal form, we
denote by $\Ap(\Pi)$ the least approximate normal form $\bp$ of $\s$ such that
$\Pi\dem \Gam \der \bp:\tau$.   Formally:

\begin{definition}\label{def:approx}
Given $\Pi\demnf\Gam \der \s:\sig$, the approximate normal form 
$\Ap(\Pi)\in\Ap(\s)$, called the {\it approximant of $\Pi$},  is defined by induction on the structure of $\Pi$ as follows:
\begin{itemize}
\item If the last rule of $\Pi$ is $(\axvar)$, then $\Pi\dem \Gam \der \x:\sig$, and $\Ap(\Pi) := \x$.
\item If the last rule of $\Pi$ is $(\arri)$, then $\sigma= \A \arrow \rho$ and $\Pi\dem\Gam \der \l \x. \s: \A \arrow \rho$ follows  from 
     $\Pi' \dem\Gam,\x:\A \der \s:\rho$, then  $\Ap(\Pi) := \lambda \x. \Ap(\Pi')$,  $\s$ being in  $\Pi'$-normal form.
\item If the last rule of $\Pi$ is $(\arre)$, $\Pi \dem \Gam + \Delta
  \der \vv\uu: \sig$ with premises $\Pi'\dem\Gam\der \vv: \A \to \sig$
  and $\Pi''\dem\Delta\der\uu:\A$, so by induction $\Ap(\Pi')
    \in \Ap(\vv)$ and $\\Ap(\Pi'') \in \Ap(\uu)$. Moreover the
  hypothesis that $\vv\uu$ is in $\Pi$-normal form implies that $\vv$ is of the form 
$\x\vv_1\ldots\vv_n$, so that
  $\Ap(\Pi):=\Ap(\Pi')\Ap(\Pi'') \in \Ap(\vv\uu)$.
\item  If the last rule of $\Pi$ is $(\many)$,  then $\Pi\dem\Gam \der \uu: \A $ 
follows from $(\Pi_i \dem\Gam_i \der \uu: \sig_i)_{\iI}$, then $\Ap(\Pi)=\bu_{\iI} \Ap(\Pi_i)$. In fact $\uparrow_{\iI}\Ap(\Pi_i )$, since $\Ap(\Pi_i )\in \Ap(\uu)$, for all $\iI$.
\end{itemize}
\end{definition}


% \begin{definition}\label{def:approx}
% Given $\Pi\demnf\Gam \der \s:\sig$,
% $\Ap(\Pi)\in\Ap(\s)$, called the {\it approximant of $\Pi$},  is defined by induction on the structure of $\Pi$ as follows:
% \begin{itemize}
% \item If the last rule of $\Pi$ is $(\axvar)$, then $\Pi\dem \Gam \der \x:\sig$, and $\Ap(\Pi) := \x$.
% \item If the last rule of $\Pi$ is $(\arri)$, then $\sigma= \A \arrow \rho$ and $\Pi\dem\Gam \der \l \x. \s: \A \arrow \rho$ follows  from 
%      $\Pi' \dem\Gam,\x:\A \der \s:\rho$, then  $\Ap(\Pi) := \lambda \x. \Ap(\Pi')$,  $\s$ being in  $\Pi'$-normal form.
% \item If the last rule of $\Pi$ is $(\arre)$, $\Pi \dem \Gam + \Delta
%   \der \vv\uu: \sig$ with premises $\Pi'\dem\Gam\der \vv: \A \to \sig$
%   and $\Pi''\dem\Delta\der\uu:\A$, so by induction $\delia{\Ap(\Pi')
%     \in \Ap(\vv)}$ and $\delia{\Ap(\Pi'') \in \Ap(\uu)}$. Moreover the
%   hypothesis that $\vv\uu$ is in $\Pi$-normal form implies that $\vv$ is
%   a ${\cal L}$ approximate normal form, so that
%   $\Ap(\Pi):=\Ap(\Pi')\Ap(\Pi'') \delia{\in \Ap(\vv\uu)}$.
% \item  If the last rule of $\Pi$ is $(\many)$,  then $\Pi\dem\Gam \der \uu: \A $ 
% follows from $(\Pi_i \dem\Gam_i \der \uu: \sig_i)_{\iI}$, then $\Ap(\Pi)=\bu_{\iI} \Ap(\Pi_i)$. In fact $\uparrow_{\iI}\Ap(\Pi_i )$, since $\Ap(\Pi_i )\in \Ap(\uu)$, for all $\iI$.
% \end{itemize}
%\end{definition}
Remark that in the last case the approximate normal form corresponding
to the case $I=\es$ is $\Omega$. Coming back to the derivation $\Pi$
in Example~\ref{ex:meas}, we have $\Ap(\Pi) = \x \y \Omega$. More
generally, given $\Pi\demnf\Gam \der \s:\sig$, the approximant $\Ap(\Pi)$ can be
obtained from $\s$ by replacing all its maximal subterms untyped in
$\Pi$ by $\Omega$.

Simple inductions on  $\Pi$ allow to show the following properties:
\begin{proposition}\label{Prop:monoton}\mbox{}
\begin{enumerate}
\item\label{monone} Let $\Pi\dem\Gamma\der \ap:\sigma$.
  If  $\ap\leq\bp$  (resp. $\ap\leq\s$)  then there exists $\Pi'$ such that
$\Pi'\dem\Gamma\der \bp:\sigma$  (resp. $\Pi'\demnf\Gamma\der \s:\sigma$) and $\Ap(\Pi')=\Ap(\Pi)$.% (resp. idem).
\item\label{montwo}
If $\Pi \dem \Gam \der \ap:\sigma$ or $\Pi \demnf \Gam \der \s:\sigma$, then    there exists
$\Pi' \dem \Gam \der \Ap(\Pi):\sigma$ and $\Ap(\Pi')=\Ap(\Pi)$.
% \item\label{monthree}
% If $\Pi \demnf \Gam \der \s:\sigma$ then    there exists
% $\Pi' \dem \Gam \der \Ap(\Pi):\sigma$ and $\Ap(\Pi')=\Ap(\Pi)$.

\end{enumerate}
\end{proposition}
% \delia{
% \proof
% By induction on $\Pi$.
% \qed

Proposition~\ref{Prop:monoton}.\ref{montwo} ensures the completeness of approximants, in the sense that 
all the typings having subject $\s$ may also 
type  some approximant of 
$\s$. 


\section{The model $\Mu$}
The type assignment system induces a relational $\lambda$-model, in the sense of ~\cite{PPR17}, where the interpretation of a term is defined as :
\[
\den{\s}=\{ (\Gamma, \sigma  \mid \Gamma \der \s:\sigma \}
\]

Let $\ccontext$ denotes a context. 

Approximants can be defined also independently from the type assignment system, in the following way:
\begin{itemize}
\item Let $\Lambda\Omega$ be $\Lambda \cup \{\Omega\}$. The reduction $\reddo : \Lambda\Omega \times \Lambda\Omega$ is defined as:
\[
\lambda x. \Omega \reddo \Omega; \quad \quad \Omega \s \reddo \Omega.
\]
$\redo $ denotes $\redb \cup \reddo$.
\item Approximants are normal forms w.r.t. the reduction $\redo$;
\item Approximants and terms can be ordered w.r.t. $\leq_\Omega$, i.e., the minimum order induced by $\Omega \leq_\Omega \s$, foe every $\s$;
\item $\Ap(\s) = \{\ap \mid \s \redbs \uu, \ap \leq_\Omega \uu  \}$
\end{itemize}
The interpretation of approximants in the model can be defined in a similar way than terms, \ie $\den{\ap}= \{ (\Gamma, \sigma  \mid \Gamma \der \ap:\sigma \}$

\begin{theorem}\label{th:approx}(Approximation)
The relational $\lambda$-model $\Mu$ satisfies the
approximation theorem, \ie\ the interpretation of a term is the union of the 
interpretations of its approximants.
\end{theorem}
The proof follows from Proposition \ref{Prop:monoton}.

\subsection{Types and head normal forms}

\begin{property}\label{prop:char}
The system $\Mu$ characterizes head normal forms.
\end{property}
\begin{proof}
From the Approximation Theorem, since all approximants are in head normal form.
\end{proof}

\subsection{Operational semantics}
To a closed head normal form $\lambda x_1...\x_n. x_i \s_1..,.\s_m$ we can associate a measure $(i,n,m)$ describing its shape.
The same measure can be applied to approximants, being approximants in head normal form. 
We will denote by $\s \in (i,n,m)$ the fact that $\s$ reduces to a head normal form of the measure $(i,n,m)$.
\begin{definition}
The $\om$-operational semantics is defined as:
\[
\s \equm \uu \mbox{ if and only if } (\forall \mbox{ closing } \ccontext. \ccontext(\s) \in (i,n,m) \Leftrightarrow \ccontext(\uu)\in (i,n,m))
\]
\end{definition}

\begin{lemma}(correctness)
$\den{\s} =\den{\uu}$ implies $\s \equm \uu$.

\end{lemma}
\begin{proof}
By contrapposition.
Recall that denotational intepretation is contextually closed, since the properties of relational models \cite{paolini, Piccolo, RDR}, so $\den{\ccontext(\s) } \not= \den{\ccontext(\uu)}$ implies
$\den{\s} \not =\den{\uu}$ (the proof is obvious by contraposition).\\

Let $\s \not\equm \uu$. Then there is a context $\ccontext$ such that:
$\ccontext(\s) \in (i,n,m)$ and $\ccontext(\uu) \not\in (i,n,m)$ (or vice-versa).
(remember subject expansion).
There are some cases.
\begin{itemize}
\item $\ccontext(\s) \in (i,n,m)$ while $\ccontext(\uu)$ has not head normal form. By \ref{prop:char}.1 and subject expansion, $\ccontext(\s)$ can be typed while
$\ccontext(\uu)$ cannot be typed,
so $\den{\ccontext(\s) } \not= \den{\ccontext(\uu)}$. \item Let $\ccontext(\s) \in (i,n,m)$ and $\ccontext(\uu) \in (j,p,q)$. \\
Let $i\not=j$. So $x_i: \underbrace{\emul \arrow...\arrow \emul}_m \arrow \alpha \der
\ccontext(\s) x_1..x_n: \alpha$ (where $\alpha$ is a type constant), while in the type context $\{x_i: \underbrace{\emul \arrow...\arrow \emul}_m \arrow \alpha\}$ $\ccontext(\uu)$ cannot be typed,
since its head is not typed. So $\den{\ccontext(\s) } \not= \den{\ccontext(\uu)}$. \\
Let $i=j, n < p,m=q$. Then $\ccontext(\s)x_1...x_n \redbs x_i \vv_1...\vv_m$ while $\ccontext(\uu)x_1...x_n \redbs \lambda x_{n+1}...x_p.x_i \vv'_1...\vv'_m$,
so the same type context as before separates $\ccontext(\s)$ and $\ccontext(\uu)$.\\
Let $i=j, n=p, m<q $. Then $\ccontext(\s)x_1...x_n \redbs x_i \vv_1...\vv_m$ while $\ccontext(\s)x_1...x_n \redbs x_i\vv'_1...\vv'_q$,
so the same type context as before separates $\ccontext(\s)$ and $\ccontext(\uu)$.

\end{itemize}
\end{proof}

We will prove that the model is also fully abstract w.r.t. the ${\tt M}$-operational semantics.
To do so, we need a semi-separability property for approximants.
Let extend to approximants the notation introduced for operational semantics, \ie $\ap \in (i,m,n) $.
\begin{lemma}(Semi-separability)
Let $\ap \not \leq_\Omega \ap'$. Then there is a context $\ccontext$ such that $\ccontext(\ap) \redo^* \ap_1 \in (i,n,m)$ while
$\ccontext(\ap')$ either has not head normal form or $\redo^* \ap_2 \not\in (i,n,m)$.
\end{lemma}

The proof is supplied by the following algorithm, which is a minor modification of the classical semi-separability algorithm (see \cite{PaoliniRonchi book}).

\medskip
The algorithm $S$ is such that, if $\ap, \ap'$ are closed, $\ap \not \leq_\Omega \ap'$ then $S(\ap,\ap') = \ccontext$, where $\ccontext$ is a semi-separating context for $\ap,\ap'$.
Let $U^m_k=\lambda x_1...x_m.x_k$, $r > max \{Arg(\ap), Arg(\ap')\}$, where $Arg (\ap)$ denotes the maximum number of arguments in all the subterms of $\ap$,
$B^m=\lambda x_1...x_{m+1}.x_{m+1}x_1...x_m$. $nf(\vv)$, where $\vv \in \Lambda\Omega$, denotes its $\beta\Omega$.normal form.

\medskip

\[
\infer{}{S(\ap, \Omega)= \Box}
\]
\medskip
\[
\infer{i\not=j \quad or \quad n\not=p \quad or \quad m \not = q}{S(\lambda x_1..x_n.x_i \ap_1...\ap_m, \lambda x_1..x_p.x_j \ap'_1...\ap'_q)= \Box}
\]
\medskip
\[
\infer{\ap_k \not\leq_\Omega \ap'_k \quad x_i \not\in \fv{\ap_k} \cup \fv{\ap'_k} \quad S(\lambda x_1..x_n.\ap_k, \lambda x_1..x_n.\ap'_k )= \ccontext_k}
{S(\lambda x_1..x_n.x_i \ap_1...\ap_m, \lambda x_1..x_n.x_i \ap'_1...\ap'_m)= \ccontext_k(\Box x_1..x_{i-1} U^m_k x_{i+1}...x_n)}
\]
\medskip
\[
\infer{
%\begin{array}{l}
\ap_k \not \leq_\Omega \ap'_k \\
 x_i \in \fv{\ap_k} \cup \fv{\ap'_k} \\
\ccontext^i_k = \Box x_1...x_{k-1} B^r x_{k+1}..x_n \\
S(nf(\lambda x_1..x_n.\ccontext^i_k(\ap_k), nf(\lambda x_1..x_n.\ccontext^i_k(\ap'_k) )= \ccontext_k
%\end{array}
}
{S(\lambda x_1..x_n.x_i \ap_1...\ap_m, \lambda x_1..x_n.x_i \ap'_1...\ap'_m)= \ccontext_k(\lambda x_1..x_n.\ccontext^i_k x_1...x_{k-1} B^r x_{k+1}..x_n )}
\]

(For the proof, see the book RonchiPaolini, pag.160. But this case is simpler!)

\begin{lemma}(Completeness)
$\s \equm \uu$ implies $\den{\s}=\den{\uu}$.
\end{lemma}
\begin{proof}
By contraposition. $\den{\s} \not=\den{\uu}$ implies there are $\ap \in \Ap(\s)$ and $\ap'\in \Ap(\uu)$ such that $\ap \not\leq_\Omega \ap'$.
Let $\ap_1 $ and $\ap_2$ be the closure of $\ap$ and $\ap'$ respectively. Obviously $\ap_1 \not\leq_\Omega \ap_2$. By the semi-separability lemma,
there is a context $\ccontext$ such that $\ccontext(\ap_1)\in (i,n,m)$ and $\ccontext(\ap_2)\not \in (i,n,m)$, for some $i,n,m$. 
Since approximants of a term are obtained by replacing subterms by $\Omega$, the same context will semi-separate $\s$ and $\uu$,
so
$\s \not \equm\uu$.
\end{proof}

\begin{theorem}
The model is fully abstract w.r.t. the ${\tt M}$-operational semantics.
\end{theorem}

Moreover, consider the standard notion of Bohm tree. $BT(\s)$ is the sup of $\Ap(\s)$, so 
$\den{\s}=\den{\uu}$ iff $BT(\s)=BT(\uu)$. So it turns out that the model of De Carvalho is the relational version of the model ${\mathcal B}$ of Barendregt (see \cite{Barendregt}, pag. 486), for which Barendregt did not supply a correspondence with an operational semantics.
\end{document}

