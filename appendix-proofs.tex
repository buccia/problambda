
\section{Some Technical Proofs}

\paragraph{Subject Reduction.}

\delia{
  \begin{lemma}[Split] \label{lem:split}
  \begin{itemize}
    \item Let $\Pi \dem \Gamma \der M:\ma$.
  Then for any decomposition $\ma = +_{\iI} \ma_i$
  there are $\Pi_i\dem \Gamma_i \der M:\ma_i$
  such that $|\Pi| = \sum_{\iI} |\Pi_i|$ and $\Gamma = \uplus_{\iI} \Gamma_i$. 
\item Let $\Pi_i\dem \Gamma_i \der M:\ma_i$ for $\iI$.
  Then there is $\Pi \dem \Gamma \der M:\ma$ such that
  $|\Pi| = \sum_{\iI} |\Pi_i|$,  $\Gamma = \uplus_{\iI} \Gamma_i$
  and $\ma = +_{\iI} \ma_i$. 
  \end{itemize}
\end{lemma}
}

As usual, the subject reduction property relies on a substitution
property.
\begin{lemma}[Substitution] \label{lem:sub}
  $\Pi \dem \Gamma, x:\ma \der M:\at$ (resp. $\Pi \dem \Gamma, x:\ma
  \der M:\mb$) and $\Theta\dem \Delta \der N:\ma$ imply $\Pi[\Theta/x]\dem
  \Gamma \uplus \Delta \der M[N/x]:\at$ (resp. $\Pi[\Theta/x]\dem \Gamma
  \der M[N/x]:\mb$).  Moreover, $\size{\Pi[\Theta/x]} < \size{\Pi} +
  \size{\Theta}$.
\end{lemma}
\begin{proof}
By induction on $\Pi$:
\begin{itemize}
\item
  Let $\Pi$ be:
  \[
  \infer[\rulevar]{\Gamma, y:\ma  \der y:\at}{a\in \ma}
  \]
  On the other side, the derivation $\Theta$ is of the shape:
  \[
  \infer[\rulebang]{\uplus_{\iI}\Delta_i\der N:\mul{\at_{i}\st \iI}}{(\Pi_i\dem\Delta_i\der N: \at_{i})_{\iI}}
  \]
  where $M=y$, $\Delta = \uplus_{\iI}\Delta_i$ and $\ma= \mul{\at_{i}\st \iI}$.
  There are two cases. If  $y=x$, then $M[N/x]=N$ and $\at=\at_i$, for some $i$.
  By Property \ref{prop:weak},
  there is a derivation $\Xi\dem\Gamma \uplus_{\iI} \Delta_i \der N:
  \at_{i}$, such that $\size{\Xi}=\size{\Pi_i}$. So $\Pi[\Theta/x] =
  \Xi$.  If $y\not= x$, \delia{then $M[N/x]=y$, and $\Pi' \dem \Gamma\der y:\at$ is
  trivially derivable
  with $|\Pi'| = |\Pi|$. Moreover, by Property \ref{prop:weak},
  there is a derivation $\Xi\dem\Gamma \uplus \Delta \der y:
  \at$, such that $\size{\Xi}=\size{\Pi'}=\size{\Pi}$.} 
  Then $\Pi[\Theta/x]=\cdelia{\Pi}{\Xi}$. In both cases the
  condition on the size of $\Pi[\Theta/x]$ is obvious.
\item
  Let $\Pi$ be:
  \[
  \infer[\ruleapp]{\Gamma' \uplus_{\iI}\Gamma_i, x:(\ma'+_{\iI}\ma_i )\der PQ: \dist{p_i\Bt_i \mid i\in I}}
	  {\Pi'\dem \Gamma', x:\ma' \der P: \dist{p_i(\mb_i \arrow \Bt_i) \mid \iI} &  (\Pi_i\dem \uplus_{\iI}\Gamma_i, x:\ma_i\der Q: \mb_i)_{\iI}	}
   \]
   where \delia{$\Gamma= \Gamma' \uplus_{\iI}\Gamma_i$, $\ma = \ma' +_{\iI}\ma_i$ and
   $\at= \dist{p_i\Bt_i \mid i\in I}$}.
   \delia{ By using Lemma~\ref{lem:split} we can split derivation $\Theta$
     in the following  derivations:}
   \[ \Theta_{\ma'}\dem\Delta_{\ma'} \der N: \ma' \quad\quad
      (\Theta_{\ma_i} \dem\Delta_{\ma_i}\der N: \ma_i)_{\iI}    \]
   where $\Delta= \Delta_{\ma'} \uplus_{\iI}\Delta_{\ma_i}$,
   $\ma = \ma' \uplus_{\iI} \ma_i$ and $|\Theta| = |\Theta_{\ma'}| +_{\iI} |\Theta_{\ma_i}|$.

  \delia{ The \ih\ gives derivations $\Pi'[\Theta_{\ma'}/x]$ and $(\Pi_i[\Theta_{\ma_i}/x])_{\iI}$, with subject
   $P[N/x]$ and $Q[N/x]$ resp. } Since
   $(PQ)[N/x]=P[N/x]Q[N/x]$ the result follows by rule $(\ruleapp)$.
   Moreover by the \ih\  $\size{\Pi'[\Theta_{\ma'}/x]}< \size{\Pi'}+\size{\Theta_{\ma'}}$,
   and $\size{\Pi_i[\Theta_{\ma_i}/x]}< \size{\Pi_i}+\size{\Theta_{\ma_i}}$, so
   that $\size{\Pi[\Theta/x]}=
   \size{\Pi'[\Theta_{\ma'}/x]}+_{\iI}\size{\Pi_i[\Theta_{\ma_i}/x]}+1
   <\size{\Pi'}+\size{\Theta_{\ma'}} +_{\iI} ( \size{\Pi_i}
   +\size{\Theta_{\ma_i}})+1=\size{\Pi}+ \size{\Theta}$. 
 \item
   Let $\Pi$ be:
   \[
   \infer[\ruleprob]{\Gamma_1 \uplus\Gamma_2, x:\ma_1 + \ma_2  \der P\oplus Q: \two \mbf_1 \dsum \two \mbf_2}
         {\Pi_1\dem\Gamma_1, x:\ma_1 \der P: \mbf_1 &
           \Pi_2\dem\Gamma_2, x:\ma_2 \der Q: \mbf_2 & \mbf_1 + \mbf_2 \neq \emul}
         \]
\delia{    By using Lemma~\ref{lem:split} we can split derivation $\Theta$
     in the following  derivations:}
   \[ \Theta_{1}\dem\Delta_{1} \der N: \ma_1 \quad\quad
   \Theta_{2} \dem\Delta_{2}\der N: \ma_2    \]
   where $\Delta= \Delta_{1} \uplus\Delta_{2}$,
   $\ma = \ma_1 \uplus \ma_2$ and
   $|\Theta| = |\Theta_{1}| + |\Theta_{2}|$.
   By the \ih\ there are derivations $\Pi_1[\Theta_1/x] \dem \Gamma_1 \uplus \Delta_1
   \der P[N/x]:\mbf_1$ and $\Pi_2[\Theta_2/x] \dem \Gamma_2 \uplus \Delta_2
   \der Q[N/x]:\mbf_2$, so that $\Pi[\Theta/x]$ can be built by rule
   $(\ruleprob)$. Moreover
   \delia{$\size{\Pi[\Theta/x]}=\size{\Pi_1[\Theta_1/x]} + \size{\Pi_2[\Theta_2/x]} + 1
    <_{\ih} \size{\Pi_1} +\size{\Theta_1} +  
   \size{\Pi_2}+\size{\Theta_2} +1=  \size{\Pi} + \size{\Theta}$.}
 \item
   If the last used rule is $(\rulelam)$, 
   the proof follows by induction.
\item
   \delia{   If the last used rule is $(\rulebang)$, 
   the proof follows by induction.}
\end{itemize}
\end{proof}
Let us identify an occurrence of the  term $N$ in a term $M$ by the
context $\cc$ such that $M=\cc(N)$.  Then, given a typing derivation
$\Pi\dem \Gamma\der M:\at$, an occurrence of a subterm of $M$ is a
typed occurrence of $\Pi$ if and only if it is the subject of some 
subderivation of $\Pi$.
More precisely,
given a type derivation $\Pi$, the set of
typed occurrences of $\Pi$, written $\toc{\Pi}$, 
is defined by induction on the last rule  of $\Pi$. 

\begin{itemize}
\item If $\Pi$ ends with $(\rulevar)$,   then 
$\toc{\Pi} := \set{\Box}$.
\item If $\Pi$ ends with $(\rulelam)$ 
with subject $\lambda x.M$ and 
premise $\Pi'$  then \\
$\toc{\Pi} := \set{\square} \cup \set{ \lambda x.\cc\mid \cc \in \toc{\Pi'}}$ .
\item If $\Pi$ ends with $(\ruleapp)$ 
with subject $MN$ and 
premises $\Pi_1$ and $(\Pi_2^i)_{\iI}$
with subjects  $M$
and $N$ respectively, then 
$\toc{\Pi} := \set{\square} \cup \set{M \cc\mid \delia{\cc \in \toc{\Pi^i_2} \mbox{ and } \iI }}
\cup \set{\cc N \mid \cc \in \toc{\Pi_1}}$. 
\item  If $\Pi$ ends with $(\ruleprob)$ with subject $M\oplus N$ and
 premises  $\Pi_1$ and $\Pi_2$
with subjects $M$ and  $N$ respectively, then $\toc{\Pi} := \set{\square} \cup
 \set{\cc \oplus N\mid \cc \in \toc{\Pi_1}} \cup 
 \set{M \oplus\cc \mid \cc  \in \toc{\Pi_2} }$.
 \item If $\Pi$ ends with $(\rulebang)$, with premises $\Pi_k\ (k \in K)$, then
 $\toc{\Pi} := \cup_{\kK } \toc{\Pi_k}$.
\end{itemize}
\begin{lemma*}[\ref{lem:one-subred}, One-Step Subject Reduction]
Let $\Pi\dem \Gamma \der M:\at$.
\begin{enumerate}
\item If $M \redb \dist{N}$ then there is $\Psi\dem \Gamma \der N:\at$.
\item If $M \redo \dist{\two M_1,\two M_2}$, then
  $\Psi_1 \dem \Gamma_1 \der
  M_1:\mbf_1$ and $\Psi_2 \dem \Gamma_2 \der M_2:\mbf_2$,
  where $\mbf_1+\mbf_2 \neq \emul$, $\at=\two \mbf_1 +\two \mbf_2$ and $\Gamma = \Gamma_1 \uplus \Gamma_2$.
 \end{enumerate}
Moreover, if the redex is typed in $\Pi$, then $\size{\Psi} <
  \size{\Pi}$ (resp. $\size{\Psi_i}<\size{\Pi}$).
\end{lemma*}
\begin{proof} \mbox{}
\begin{enumerate}
\item
  By induction on the context $\cc$ such that $M=\cc((\lambda x.P)Q)$
  and $M'=\cc(P[Q/x])$. The base case follows by Lemma \ref{lem:sub},
  the inductive cases are easy.
\item
  By induction on the context $\ss$ such that $M=\ss(P\oplus Q)$, and
  then by induction on $\Pi$. \delia{discuter}
  \begin{enumerate}
  \item Let $\ss=\square$. Then the last rule of $\Pi$ is $\oplus$,
    and the proof is obvious.

    \item Let $\ss=\lambda x. \ss'$. Then 
    $M=\lambda x. N \redo \dist{\two \lambda x. N_1, \two \lambda
      x. N_2}$ implies  $N \redo \dist{\two N_1, \two N_2}$.
    The derivation $\Pi$ is
    of the shape:
    \[
    \infer[\rulelam]{\Gamma \der \lambda x.N: \dist{p_i(\ma \arrow \At_i) \mid \iI} = \at}{\Pi' \dem\Gamma, x:\ma \der N:\dist{p_i \At_i \mid \iI}}
    \]
    By the \ih\ there are derivations $\Pi'_1 \dem \Gamma_1, x:\ma_1 \der
    N_1:\mcf_1$, $\Pi'_2 \dem \Gamma_2, x:\ma_2 \der N_2:\mcf_2$, where
    $\mcf_1 + \mcf_2 \neq \emul$, $\dist{p_i
      \At_i \mid \iI}= \two \mcf_1+ \two \mcf_2$, and $\Gamma, x:\ma =
    (\Gamma_1, x:\ma_1) \uplus (\Gamma_2, x:\ma_2)$, so that $\Gamma
    = \Gamma_1 \uplus \Gamma_2$ and $\ma = \ma_1 + \ma_2$. Moreover,
    if the redex is typed, then
    $\size{\Pi'_h}  <_{\ih} \size{\Pi'}$ for $h=1,2$.
    By definition, for each $h=1,2$,
    we have $\mcf_h = \mul{\at_j}_{\jJ_h}$
    where $|J_h| \leq 1$.
    \begin{itemize}
      \item If $|J_h| = 0$, \ie\ $\mcf_h= \emul$, then $\Gamma_h = \emptyset$,
        and $\ma_h = \emul$. Let $\mbf_h = \mcf_h = \emul$.
        We construct $\Psi_h$  as follows:
    \[  \infer[\rulebang]
        {\Gamma_h  \der \lambda x. N_h: \mbf_h}
        {}
        \]
        We have $\size{\Psi_h} = 0 < \size{\Pi}$. 
        \item  If $|J_h| = 1$, let us call $h$ the unique
    index of $J_h$ so that $\mcf_h = \mul{\at_h}$. Using Lem.~\ref{lem:split} we
    obtain a derivation $\Pi''_h \dem \Gamma_h, x:\ma_h \der
    N_h:\at_h$ such that $\size{\Pi'_h} = \size{\Pi''_h}$.
    Since $\dist{p_i  \At_i \mid \iI}= \two \mcf_1+ \two \mcf_2$, then 
    $\at_h = \dist{2p_i \At_i \mid \iI_h}$
    where $I = I_1 \uplus I_2$.
    Moreover, we can apply 
    rule $(\rulelam)$ to obtain $\overline{\Psi}_h \dem \Gamma_h
    \der \lambda x. N_h:\dist{2p_i(\ma_h \arrow \At_i) \mid \iI_h}$
    where $\size{\overline{\Psi}_h} = \size{\Pi''_h} +1 $. 
    Let $\ct_h = \dist{2p_i(\ma_h \arrow \At_i) \mid \iI_h}$,
    and $\mbf_h = \mul{\ct_h}$.
    Then $\Psi_h$ is constructed as follows:
    \[  \infer[\rulebang]
        {\Gamma_h  \der \lambda x. N_h: \mul{\ct_h}}
        {\overline{\Psi}_h \dem \Gamma_h  \der \lambda x. N_h:\ct_h}
        \]
    \end{itemize}
    Since $\mcf_1 + \mcf_2 \neq \emul$, then
    for some $h=1,2$,  we have $|J_h|=1$, so that
    $\mbf_h \neq \emul$. 
    Thus
    $\mbf_1+\mbf_2 \neq \emul$ as required.
    On the other hand $\dist{p_i
      \At_i \mid \iI}= \two \mcf_1+ \two \mcf_2$
    implies $\at= \dist{p_i (\ma \arrow \At_i) \mid \iI}= \two \mbf_1+ \two \mbf_2$.
    Moreover, if
    the redex is typed in $\Pi$, then it is typed in $\Pi'$,
    so we have $\size{\Psi_h} = \size{\overline{\Psi}_h} = \size{\Pi''_h} +1= \size{\Pi'_h} +1 <_{\ih} \size{\Pi'} +1 = \size{\Pi}$. \delia{ceci ne marche pas si on autorise
      plusieurs types dans le multiset de la regle $\ruleprob$}

    \item  Let $\ss=\ss' P$. Then $M=NP \redo
    \dist{\two N_1P, \two N_2P}$ implies $N \redo \dist{\two N_1, \two N_2}$ and $\Pi$ is of the shape:
    \[
    \infer[\ruleapp]{\Gamma=\Delta \uplus_{\iI} \Gamma_i \der NP:\dist{p_i\At_i \mid \iI} = \at}{\Pi' \dem\Delta \der N: \dist{p_i(\ma_i \arrow \At_i)\mid \iI } 
      \quad (\Pi_i \dem\Gamma_i \der P:\ma_i)_{\iI}}
    \]

By the \ih\ there are derivations $\Pi'_1 \dem \Delta_1 \der
    N_1:\mcf_1$, $\Pi'_2 \dem \Delta_2 \der N_2:\mcf_2$, where
    $\mcf_1 + \mcf_2 \neq \emul$, $\dist{p_i(\ma_i \arrow
      \At_i) \mid \iI}= \two \mcf_1+ \two \mcf_2$, and $\Delta=
    \Delta_1 \uplus \Delta_2$.
    Moreover,
    if the redex is typed, then
    $\size{\Pi'_h}  <_{\ih} \size{\Pi'}$ for $h=1,2$.
    By definition, for each $h=1,2$,
    we have $\mcf_h = \mul{\at_j}_{\jJ_h}$
    where $|J_h| \leq 1$.
    \begin{itemize}
      \item If $|J_h| = 0$, \ie\ $\mcf_h= \emul$, then $\Delta_h = \emptyset$. Let $\mbf_h = \mcf_h = \emul$.
        We construct $\Psi_h$  as follows:
    \[  \infer[\rulebang]
        {\Gamma_h  \der N_hP: \mbf_h}
        {}
        \]
        We have $\size{\Psi_h} = 0 < \size{\Pi}$. 
        \item  If $|J_h| = 1$, let us call $h$ the unique
    index of $J_h$ so that $\mcf_h = \mul{a_h}$. Using Lem.~\ref{lem:split} we
    obtain a derivation $\Pi''_h \dem \Delta_h \der
    N_h:\at_h$ such that $\size{\Pi'_h} = \size{\Pi''_h}$.
    Since $\dist{p_i(\ma_i \arrow
      \At_i) \mid \iI}= \two \mcf_1+ \two \mcf_2$, then 
    $\at_h = \dist{2p_i(\ma_i \arrow \At_i) \mid \iI_h}$
    where $I = I_1 \uplus I_2$.
    Moreover, we can apply 
    rule $(\ruleapp)$ to $(\Pi_i \dem \Gamma_i \der P:\ma_i)_{\iI_h}$ to obtain $\overline{\Psi}_h \dem \Delta_h \uplus_{\iI_h} \Pi_i 
    \der N_hP:\dist{2p^i\At^i}_{\iI_h}$
    where 
    $\size{\overline{\Psi}_h} = \size{\Pi''_h} +_{\iI_h} \size{\Pi_i} + 1 $. 
    Then $\Psi_h$ is constructed as follows, where $\mbf_h=\mul{\dist{2p^i\At^i}_{\iI_h}}$:
    \[  \infer[\rulebang]
        {\Delta_h  \uplus_{\iI_h} \Pi_i \der N_hP: \mul{\dist{2p^i\At^i}_{\iI_h}}}
        {\overline{\Psi}_h \dem \Delta_h \uplus_{\iI_h} \Pi_i  \der N_hP:\dist{2p^i\At^i}_{\iI_h}}
        \]
    \end{itemize}
    
    Since $\mcf_1 + \mcf_2 \neq \emul$, then
    for some  $h=1,2$,  we have $|J_h|=1$, so that
    $\mbf_h \neq \emul$.
    Thus
    $\mbf_1+\mbf_2 \neq \emul$ as required.
    On the other hand $\dist{p_i (\ma \arrow \At_i) \mid \iI}= \two \mcf_1+ \two \mcf_2$
    implies $\at= \dist{p_i
      \At_i \mid \iI}= \two \mbf_1+ \two \mbf_2$.
    Moreover, if the redex is typed in $\Pi$, then it is
    typed in $\Pi'$, so  $\size{\Psi_h}=\size{\overline{\Psi_h}}=
    \size{\Pi''_h} +_{\iI_h}\size{ \Pi_i} +1 =
    \size{\Pi'_h} +_{\iI_h}\size{ \Pi_i} +1  < \size{\Pi'}
    +_{\iI}\size{ \Pi_i} +1=\size{\Pi}.  $
  \end{enumerate}
\end{enumerate}
\end{proof}
\paragraph{Subject Expansion.}
\begin{lemma}[Inverse Substitution]\label{lem:inv}
 $\Pi \dem\Gamma \der M[N/x]:\at$ 
implies there is $\ma$ such that $\Sigma, x:\ma \der M:\at$, $\Delta \der N:\ma$ and $\Gamma \subseteq \Sigma \uplus \Delta$. 
\end{lemma}
\begin{proof}
By induction on $M$.
All the cases follow easily by induction. Note that, in case all the occurrences of $N$ in $M$ are untyped in $\Pi$, then $\ma=[\;]$ and $\Sigma=\Gamma$.
\end{proof}
\begin{lemma}[One-Step Subject Expansion]\label{lem:exp}
\begin{enumerate}
\item $\Pi \dem\Gamma \der M:\at$ and $N \redb \dist{M}$ imply $\Gamma \der N:\at$.
\item $\Pi \dem\Gamma \der M: \at$ and $N \redo \dist{\two M, \two P}$ imply $\Gamma \red N: \two\at$.
\item $\Pi \dem \Gamma \der M_i :\at_i$ for every $1\leq i\leq 2$ and $N \redo \dist{\two M_1, \two M_2}$ imply $\Gamma \der N: \two\at_1 + \two \at_2$.
\end{enumerate}
\end{lemma}
\begin{proof}
\begin{enumerate}
\item
  By Lemma \ref{lem:inv}.
\item
  By induction on the context $\ss$ such that $N=\ss(R_1\oplus R_2)
  \redo \dist{\two\ss(R_1), \two \ss(R_2)}$ and either
  $M=\ss(R_1)$ or $P=\ss(R_2)$, and then by induction on $\Pi$.  In
  case $\ss = \square$, the proof is obvious. Let $\ss=\lambda
  x. \ss'$, so $N= \lambda x.Q \redo \dist{\two\lambda x.N_1,
    \two \lambda x. N_2}$, where $Q \redo \dist{\two N_1, \two
    N_2}$. Let $\Pi \dem\Gamma \der \lambda x.N_1:\at$. Then $\Pi$ is
  of the shape:
  \[
  \infer[\rulelam]{\Gamma \der \lambda x.N_1:\dist{p_i(\ma \arrow \Bt_i \mid \iI}}{\Gamma, x:\ma \der N_1:\dist{p_i \Bt_i \mid \iI} } 
  \] 
  By induction $\Gamma, x:\ma \der Q:\dist{\two p_i \Bt_i \mid \iI}$,
  and then, by rule (\rulelam), $\Gamma \der \lambda x. Q:
  \dist{\two p_i(\ma \arrow \Bt_i) \mid \iI}$. Let $\ss=\ss' R$, so
  $N= QR\redo \dist{\two N_1 R, \two N_2 R}$. Then $\Pi$ is of
  the shape:
  \[
  \infer[\ruleapp]{\Gamma \uplus_{\iI} \Delta_i \der N_1R:\dist{p_i\At_i \mid \iI}}{\Gamma \der N_1:\dist{p_i(\ma_i \arrow \At_i )\mid \iI}\quad (\Delta_i \der R: \ma_i)_{\iI}} 
  \]  
  By induction, $\Gamma \der Q:\dist{\two p_i(\ma_i \arrow \At_i)
    \mid \iI}$, so the proof follows by rule (\ruleapp).
\end{enumerate}
\end{proof}


